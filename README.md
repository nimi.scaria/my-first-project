# Assignment 2 Workflow

S1262 Assignment 2 is a simple project to learn the basics of Git.

## Prerequisites
- Git
- Rust
## Tasks
1. Create a Repository in TU Clausthal Gitlab
1. Update the license and README.md
1. Clone the Repository
1. Init Rust project and Gitignore
1. Create Dev branch in localy cloned repository
1. Merge the Dev branch to Master
1. Submit the Assignment

### LICENSE
This project is licensed under the MIT License. This license scheme is entirely compatible with RUST. MIT License support Commercial use, Modification of work, Distribution of source code and Private Use. For more information take a look at the  [LICENSE](https://gitlab.tu-clausthal.de/ns20/s1262-assignment-2/-/blob/master/LICENSE "LICENSE") file.
